import './App.css';
import { useSelector, useDispatch } from "react-redux";
import { useEffect } from 'react';
import { fetchUser } from './redux/action';
import * as React from 'react';

function App() {
  let dispatch = useDispatch();
  const data = useSelector((state) => state.details);

  useEffect(() => {
    debugger
    dispatch(fetchUser())
  },[])

console.log(data)
  return (
    <div className="App">
  {data.map((val)=>(<li>{val.name}</li>))}
    </div>
  );
}

export default App;
