import { CREATE } from "./actionType";

const initialState = {
    details: []
}
function reducer(state = initialState, action) {
    debugger
    switch (action.type) {
        case CREATE: return {
            ...state,
            details : action.payload
        };
        default:
            return state;
    }
}

export default reducer;
